<?php
//fetch.php
$connect = mysqli_connect("localhost", "root", "", "skladiste_db");
$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM artikli 
  WHERE sifra LIKE '%".$search."%'
  OR naziv LIKE '%".$search."%' 
 ";
}
else
{
 $query = "
  SELECT * FROM artikli ORDER BY sifra
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
 <table class="table" id="tableOfArticles">
          <thead>
          <tr>
               <th scope="col">Šifra</th>
               <th scope="col">Naziv artikla</th>
               <th scope="col">Količina</th>
               <th scope="col">Detalji</th>
          </tr>
          </thead>
         
     <tbody>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $output .= '
    <tr>
        <td scope="row">'.$row["sifra"].'</td>
        <td>'.$row["naziv"].'</td>
        <td>'.$row["kolicina"].'</td>
        <td><input type="button" data-target="dataModal" name="view" value="Detalji" id="' . $row["sifra"] . '" class="btn btn-info btn-xs view_data" /></td>  
    </tr>
  ';
 }
 $output .= '</tbody> </table>';
 echo $output;
}
else
{
 echo 'Ne postoji artikl s traženim nazivom ili šifrom';
}

?>