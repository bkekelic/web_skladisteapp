
<?php  
//index.php
// Initialize the session
session_start();

// Check if the user is not already logged in, if not then redirect him to login page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
}else{
    header("location: login.php");
    exit;
}

$connect = mysqli_connect("localhost", "root", "", "skladiste_db");
$query = "SELECT * FROM artikli";
$result = mysqli_query($connect, $query);
 ?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Skladiste App</title>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>

<body>
    <!-- Navigration bar -->
    <nav class="navbar navbar-default">

        <div class="container">

            <!-- Logo -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.php" class="navbar-brand">SKLAPP</a>


            </div>

            <!-- Menu Items -->
            <div class="collapse navbar-collapse" id="mainNavBar">
                <ul class="nav navbar-nav">
                        <li class="active"> <a href="index.php">Pretraži</a></li>
                        
                        <?php

                              $id = $_SESSION["id"];
                              $queryAdmin = "
                                  SELECT admin FROM zaposlenici WHERE id = $id 
                              ";
                              $resultAdmin = mysqli_query($connect, $queryAdmin);
                              $rowAdmin = mysqli_fetch_array($resultAdmin);

                              if($rowAdmin["admin"] == '1'){
                                  ?>
                                  <li> <a href="register.php">Registriraj novog korisnika</a></li>
                                  <?php
                              }
                              ?>   

                        <li>
                          <a href="#" class="dropdown-toggle" id="myDropdown" data-toggle="dropdown">Dodatne opcije <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li> <a href="#" data-toggle="modal" id="myDropdownItem" data-target="#dodajArtiklModal">Dodaj artikl</a></li>
                            <li> <a href="#" data-toggle="modal" id="myDropdownItem" data-target="#izmijeniProfilModal">Izmijeni profil</a></li>
                          
                          </ul>
                      </li>                                       
                </ul>
                <ul class="nav navbar-nav navbar-right">
                   <li><a href="logout.php">Odjavi se</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- TODO remove this div -->
    <div id="modalTryPrint"></div>

    
     <!-- Search button -->
    <div class ="container">
        <div class="active-cyan-3 active-cyan-4">
            <input class="form-control" type="text" placeholder="Unesite naziv ili šifru artikla" aria-label="Search" id="searchText">
        </div>
    </div>

    <!-- Table -->
    <div class="container" style="margin-top: 20px">
        <table class="table" id="tableOfArticles">        
        </table>
    </div>


  

    <!-- Footer -->
    <div  class="navbar navbar-inner navbar-fixed-bottom ">    
        <div class="footer-copyright text-center py-3">© 2019 Copyright: Bernard Kekelić
        </div>      
    </div> 

    
</body>
</html>


<!-- ***************************************************** END OF HTML FILE  -> MODALS AND SCRIPTS ************************************************************************************* -->





  <!-- Modal - dodaj artikl-->
  <div class="modal fade" id="dodajArtiklModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Dodaj novi artikl</h4>
        </div>
        <div class="modal-body"> 

            <form method="post" id="formDodajArtikl">  
                    <label>Naziv artikla</label>
                    <input type="text" name="nazivArtikla" id="nazivArtikla" class="form-control" />
                    <br />

                    <label>Cijena artikla</label>
                    <input type="text" name="cijenaArtikla" id="cijenaArtikla" class="form-control" />
                    <br />

                    <label>Količina</label>
                    <input type="text" name="kolicinaArtikala" id="kolicinaArtikala" class="form-control" />
                    <br />

                    <label>Opis artikla</label>
                    <textarea name="opisArtikla" id="opisArtikla" class="form-control"></textarea>
                    <br />

                    <input type="submit" name="insert" id="insert" value="Spremi artikl" class="btn btn-success" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>

                
                    </form>
                    </div>

        </div>
      </div>
      
    </div>
  </div>

  
    <!-- Dodaj artikl-->
    <script>  
        $(document).ready(function(){
         $('#formDodajArtikl').on("submit", function(event){  
          event.preventDefault();  
            
          if($('#nazivArtikla').val() == '')  
          {  
           alert("Potrebno je unijeti naziv artikla");  
          }  
          else if($('#cijenaArtikla').val() == '')  
          {  
           alert("Potrebno je unijeti cijenu artikla");  
          } 
          else if($('#kolicinaArtikala').val() == '')  
          {  
           alert("Potrebno je unijeti količinu artikla");  
          }
          else if($('#opisArtikla').val() == '')  
          {  
           alert("Potrebno je unijeti opis artikla");  
          }
           
          else  
          {  
           $.ajax({  
            url:"dodajArtikl.php",  
            method:"POST",  
            data:$('#formDodajArtikl').serialize(),  
            beforeSend:function(){  
             $('#insert').val("Spremanje...");  
            },  
            success:function(data){  
             $('#formDodajArtikl')[0].reset();  
             $('#dodajArtiklModal').modal('hide');  
             $('#tableOfArticles').html(data);  
            }  
           });  
          }  
         });
  
/* pogledaj podatke o artiklu */

$(document).on('click', '.view_data', function(){
      var articleID = $(this).attr("id");
      $.ajax({
      url:"getArticleDetails.php",
      method:"POST",

      data:{articleID:articleID},

      success:function(data){
        $('#article_detail').html(data);
        $('#ArticleDetailsModal').modal('show');
        bootbox.alert("Your message here…");
      }
      });
    });
    });  

/*Izmijeni artikl */

        $(document).ready(function(){
         $('#formIzmijeniArtikl').on("submit", function(event){  
          event.preventDefault();  
            var sifraArtiklaIzTablice = $(this).find("#sifraArtiklaUTablici").html();

            var stariNaziv = $(this).find("#stariNaziv").html();
            var staraCijena = $(this).find("#staraCijena").html();
            var staraKolicina = $(this).find("#staraKolicina").html();
            var stariOpis = $(this).find("#stariOpis").html();

            var noviNaziv = $('#noviNaziv').val();
            var novaCijena =  $('#novaCijena').val();
            var novaKolicina =  $('#novaKolicina').val();
            var noviOpis = $('#noviOpis').val();

            $.ajax({  
            url:"changeArticleDetails.php",  
            method:"POST",  

            data:{sifraArtikla:sifraArtiklaIzTablice,
              	stariNaziv:stariNaziv,
                staraCijena:staraCijena,
                staraKolicina:staraKolicina,
                stariOpis:stariOpis,
                noviNaziv:noviNaziv,
                novaCijena:novaCijena,
                novaKolicina:novaKolicina,
                noviOpis:noviOpis
                },  

            beforeSend:function(){  
             $('#spremiPromjeneOArtiklu').val("Spremanje...");  
            },  
            success:function(data){  
             $('#formIzmijeniArtikl')[0].reset();  
             $('#ArticleDetailsModal').modal('hide');  
             $('#modalTryPrint').html(data);
            } 
          });
         });
});  


 </script>

<!-- Modal article details -->

<div id="ArticleDetailsModal" class="modal fade">
 <div class="modal-dialog">
  <div class="modal-content">

   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Pojedinosti o artiklu</h4>
   </div>
   <form method="post" id="formIzmijeniArtikl">

      <div class="modal-body" id="article_detail">
      </div>

    </form>
    <div class="modal-footer">
    
    </div>
    
   </div>
  </div>
 </div>
</div>



 <!-- Modal - izmijeni profil-->
 <div class="modal fade" id="izmijeniProfilModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Izmijeni svoje podatke</h4>
        </div>
        <div class="modal-body"> 

            <form method="post" id="formIzmijeniProfil">
                    <label>Korisničko ime</label>
                    <input type="text" name="username" id="username" class="form-control" />
                    <br />

                    <label>Lozinka</label>
                    <input type="password" name="lozinka" id="lozinka" class="form-control" autocomplete="new-password" />
                    <br />

                    <label>Potvrdi lozinku</label>
                    <input type="password" name="potvrdenaLozinka" id="potvrdenaLozinka" class="form-control"/>
                    <br />

                    <input type="submit" name="submit" id="update" value="Spremi podatke" class="btn btn-success" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>

                
                    </form>
                    </div>

        </div>
      </div>
      
    </div>
  </div>



  
    <!-- Izmijeni profil -->
<script>  
        $(document).ready(function(){
         $('#formIzmijeniProfil').on("submit", function(event){  
          event.preventDefault();  

         
          if($('#username').val() == "")  
          {  
           alert("Unesite korisničko ime");  
          }  
          else if($('#lozinka').val() == '')  
          {  
           alert("Unesite novu lozinku");  
          }  
          else if($('#potvrdenaLozinka').val() == '')  
          {  
           alert("Potvrdite novu lozinku");  
          }     
          else if($('#potvrdenaLozinka').val() != $('#lozinka').val() ) {
            alert("Lozinke nisu jednake");  

          }          
          else  
          { 
            $.ajax({  
            url:"izmijeniProfil.php",  
            method:"POST",  
            data:$('#formIzmijeniProfil').serialize(),  
            beforeSend:function(){  
             $('#update').val("Spremanje...");  
            },  
            success:function(data){  
             $('#formIzmijeniProfil')[0].reset();  
             $('#izmijeniProfilModal').modal('hide');  
             $('#modalTryPrint').html(data); 
            }  
           });  

          }  
         });
});  
 </script>



<!-- Searching -->
<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"dohvatiArtikle.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#tableOfArticles').html(data);
   }
  });
 }
 $('#searchText').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>




