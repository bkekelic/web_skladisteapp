<?php
//insert.php  
session_start();
$connect = mysqli_connect("localhost", "root", "", "skladiste_db");

if(!empty($_POST))
{
    $sifraArtikla = mysqli_real_escape_string($connect, $_POST['sifraArtikla']);  
    $stariNaziv = mysqli_real_escape_string($connect, $_POST['stariNaziv']); 
    $staraCijena = mysqli_real_escape_string($connect, $_POST['staraCijena']); 
    $staraKolicina = mysqli_real_escape_string($connect, $_POST['staraKolicina']); 
    $stariOpis = mysqli_real_escape_string($connect, $_POST['stariOpis']); 
    $noviNaziv = mysqli_real_escape_string($connect, $_POST['noviNaziv']); 
    $novaCijena = mysqli_real_escape_string($connect, $_POST['novaCijena']); 
    $novaKolicina = mysqli_real_escape_string($connect, $_POST['novaKolicina']); 
    $noviOpis = mysqli_real_escape_string($connect, $_POST['noviOpis']); 

    $userID = $_SESSION["id"];
    $datum = date('Y-m-d H:i:s');

    if($noviNaziv!=""){
        $sifra_izmjene = 1;
       
        $queryIzmjena = "
        INSERT INTO izmjene (id_zaposlenika, sifra_artikla, sifra_vrste_izmjene, datum, nova_vrijednost, stara_vrijednost)
        VALUES ('$userID', '$sifraArtikla', '$sifra_izmjene', '$datum', '$noviNaziv', '$stariNaziv')
        ";
    
        if(mysqli_query($connect, $queryIzmjena))
        {
            $queryUpdate = "
                UPDATE artikli SET naziv = '$noviNaziv' WHERE sifra = '$sifraArtikla'
            ";
            if(mysqli_query($connect, $queryUpdate)){
                echo"Promjene uspješno spremljene";
            }
        }

    }

    if($novaCijena!=""){
        $sifra_izmjene = 2;
       
        $queryIzmjena = "
        INSERT INTO izmjene (id_zaposlenika, sifra_artikla, sifra_vrste_izmjene, datum, nova_vrijednost, stara_vrijednost)
        VALUES ('$userID', '$sifraArtikla', '$sifra_izmjene', '$datum', '$novaCijena', '$staraCijena')
        ";
    
        if(mysqli_query($connect, $queryIzmjena))
        {
            $queryUpdate = "
            UPDATE artikli SET cijena = '$novaCijena' WHERE sifra = '$sifraArtikla'
            ";
            if(mysqli_query($connect, $queryUpdate)){
                echo"Promjene uspješno spremljene";
            }
        }

    }
    if($novaKolicina!=""){
        $sifra_izmjene = 3;
       
        $queryIzmjena = "
        INSERT INTO izmjene (id_zaposlenika, sifra_artikla, sifra_vrste_izmjene, datum, nova_vrijednost, stara_vrijednost)
        VALUES ('$userID', '$sifraArtikla', '$sifra_izmjene', '$datum', '$novaKolicina', '$staraKolicina')
        ";
    
        if(mysqli_query($connect, $queryIzmjena))
        {
            $queryUpdate = "
            UPDATE artikli SET kolicina = '$novaKolicina' WHERE sifra = '$sifraArtikla'
            ";
            if(mysqli_query($connect, $queryUpdate)){
                echo"Promjene uspješno spremljene";
           }
        }

    }

    if($noviOpis!=""){
        $sifra_izmjene = 5;
       
        $queryIzmjena = "
        INSERT INTO izmjene (id_zaposlenika, sifra_artikla, sifra_vrste_izmjene, datum, nova_vrijednost, stara_vrijednost)
        VALUES ('$userID', '$sifraArtikla', '$sifra_izmjene', '$datum', '$noviOpis', '$stariOpis')
        ";
    
        if(mysqli_query($connect, $queryIzmjena))
        {
            $queryUpdate = " 
            UPDATE artikli SET opis = '$noviOpis' WHERE sifra = '$sifraArtikla' 
            ";
            if(mysqli_query($connect, $queryUpdate)){
                echo"Promjene uspješno spremljene";
            }
        }

    }


    
}
?>


