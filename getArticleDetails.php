<?php  

session_start();

if(isset($_POST["articleID"]))
{
 $output = '';
 $connect = mysqli_connect("localhost", "root", "", "skladiste_db");
 $sifraArtikla = $_POST["articleID"];
 $userID = $_SESSION["id"];

 $query = "
    SELECT * FROM artikli WHERE sifra = '$sifraArtikla'
 ";
 $result = mysqli_query($connect, $query);
 $output .= '  
      <div class="table-responsive">  
           <table class="table table-bordered">';
    while($row = mysqli_fetch_array($result))
    {
     $output .= '

        <tr>  
        <td width="20%"></td>  
        <td width="40%"><label>Tretnutna vrijednost</label></td>
        <td width="40%"><label>Nova vrijednost</label></td>
        </tr>

        <tr>  
            <td width="20%"><label>Šifra</label></td>  
            <td width="40%" id="sifraArtiklaUTablici" >'.$row["sifra"].'</td>
            <td width="40%"></td>
        </tr>

       
        <tr>  
            <td width="20%"><label>Naziv</label></td>  
            <td width="40%" id="stariNaziv">'.$row["naziv"].'</td>
            <td width="40%"> <input type="text" name="noviNaziv" id="noviNaziv" class="form-control" placeholder="Promijeni naziv"/></td>
        </tr>

       
        <tr>  
            <td width="20%"><label>Cijena</label></td>  
            <td width="40%" id="staraCijena">'.$row["cijena"].'</td>
            <td width="40%"> <input type="text" name="novaCijena" id="novaCijena" class="form-control" placeholder="Promijeni cijenu"/></td>        
        </tr>

       
        <tr>  
            <td width="20%"><label>Količina</label></td>  
            <td width="40%" id="staraKolicina">'.$row["kolicina"].'</td>
            <td width="40%"> <input type="text" name="novaKolicina" id="novaKolicina" class="form-control" placeholder="Promijeni količinu"/></td>        
        </tr>

        
        <tr>  
            <td width="20%"><label>Opis artikla</label></td>  
            <td width="40%" id="stariOpis">'.$row["opis"].'</td>
            <td width="40%"> <input type="text" name="noviOpis" id="noviOpis" class="form-control" placeholder="Promijeni opis"/></td>        
        </tr>

     ';
    }
    $output .= '
    </table>
    <input type="submit" name="updateArticle" id="updateArticle" value="Spremi promjene" class="btn btn-success" />
    <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
    </div>
    <hr>
    <div>
    <h4>Povijest uređivanja artikla</h4>
    </div>
    <hr>
    ';
    
    $query = "
        SELECT * FROM izmjene WHERE sifra_artikla = '$sifraArtikla'
    ";
    $result = mysqli_query($connect, $query);

    $output .= '<div class="table-responsive">  
    <table class="table table-bordered">';
    while($row = mysqli_fetch_array($result))
    {

        $output .= '<tr><td>';

        $timestamp = strtotime($row["datum"]);
        $formatedDate = date('d.m.Y H:i', $timestamp);
        $output .= $formatedDate .' -> Korisnik ';

        $userQuery = '
        SELECT username FROM zaposlenici WHERE id= '.$row["id_zaposlenika"];
        $resultUser = mysqli_query($connect, $userQuery);

        if(!$resultUser){
            printf(mysqli_error($connect));
        }
        $userRow = mysqli_fetch_array($resultUser);

        if($row["sifra_vrste_izmjene"] == 4){
            $output .= $userRow["username"] .' je dodao artikl.';
        }else{
            $output .= $userRow["username"] .' je promijenio ';

            $vrstaQuery = ' SELECT naziv FROM vrsta_zamjene WHERE id ='. $row["sifra_vrste_izmjene"];
            $resultVrsta = mysqli_query($connect, $vrstaQuery);      
           
            if(!$resultVrsta){
                printf(mysqli_error($connect));
            }
            $vrstaRow = mysqli_fetch_array($resultVrsta);

            $output .= $vrstaRow["naziv"] .' iz '. $row["stara_vrijednost"] .' u '. $row["nova_vrijednost"] .'.';
        } 
        $output .= '</td></tr>';

    }

    $output .= '
    </table>
    </div>
    ';
    echo $output;
}
?>